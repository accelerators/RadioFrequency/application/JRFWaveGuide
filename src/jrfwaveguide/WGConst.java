package jrfwaveguide;

/**
 *
 * @author pons
 */
public class WGConst {
  
  static final String APP_RELEASE = "3.0";
  
  static final String SYNOPTIC_FILE_NAME = "/jrfwaveguide/rf_wave_guide.jdw";
  
}
