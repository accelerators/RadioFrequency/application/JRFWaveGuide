package jrfwaveguide;

import fr.esrf.Tango.DevFailed;
import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.CommandList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.DevStateScalarEvent;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.IBooleanScalar;
import fr.esrf.tangoatk.core.ICommand;
import fr.esrf.tangoatk.core.IDevStateScalar;
import fr.esrf.tangoatk.core.IDevStateScalarListener;
import fr.esrf.tangoatk.core.IDevice;
import fr.esrf.tangoatk.core.INumberScalar;
import fr.esrf.tangoatk.core.IStringScalar;
import fr.esrf.tangoatk.core.IStringScalarListener;
import fr.esrf.tangoatk.core.StringScalarEvent;
import fr.esrf.tangoatk.core.attribute.AttributeFactory;
import fr.esrf.tangoatk.core.attribute.StringScalar;
import fr.esrf.tangoatk.core.command.CommandFactory;
import fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer;
import fr.esrf.tangoatk.widget.jdraw.SynopticProgressListener;
import fr.esrf.tangoatk.widget.jdraw.TangoSynopticHandler;
import fr.esrf.tangoatk.widget.util.ATKDiagnostic;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorHistory;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.ErrorPopup;
import fr.esrf.tangoatk.widget.util.Splash;
import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author pons
 */
public class MainPanel extends javax.swing.JFrame implements SynopticProgressListener,ModeComboListener,IDevStateScalarListener,IStringScalarListener {

  private AttributeList attList;
  private CommandList cmdList;
  static  ErrorHistory errWin;

  private Splash splash; 
  
  private boolean runningFromShell;
  private WGNetwork wgNetwork;

  private boolean            enableWrite;
  private boolean            expertMode = false;

  private ModePanel          tra1Panel;
  private ModeComboBox       TRA1_Combo;
  private INumberScalar      TRA1_mode_model;
  private IDevStateScalar    TRA1_state_model;
  private ICommand           TRA1_reset_model;
  private String             TRA1_stt = new String();
  private StringScalar       TRA1_tra_mode_model;
  private String             TRA1_tra_mode = "UNKNOWN";
  
  private ModePanel          tra2Panel;
  private ModeComboBox       TRA2_Combo;
  private INumberScalar      TRA2_mode_model;
  private IDevStateScalar    TRA2_state_model;
  private ICommand           TRA2_reset_model;
  private String             TRA2_stt = new String();
  private StringScalar       TRA2_tra_mode_model;
  private String             TRA2_tra_mode = "UNKNOWN";
 
  // Wageguide PLC
  private JPanel             srrfInfoPanel;  
  private BooleanColorViewer plcRemoteViewer;
  private BooleanColorViewer transModeErrorViewer;
  private ICommand           sendSRRFModel;

  // SSA
  private JPanel             c25InfoPanel;
  private BooleanColorViewer enable230VC25Viewer;
  private BooleanColorViewer powerErrorViewer;
  private SimpleScalarViewer remoteStatusViewer;
  
  private ICommand           sendC25Model;
  
  private SSAModePanel       C251Panel;
  private ModeComboBox       C251_Combo;
  private INumberScalar      C251_mode_model;
  private IDevStateScalar    C251_state_model;
  private ICommand           C251_reset_model;
  private String             C251_stt = new String();
  private StringScalar       C251_tra_mode_model;
  private String             C251_tra_mode;

  private SSAModePanel       C252Panel;
  private ModeComboBox       C252_Combo;
  private INumberScalar      C252_mode_model;
  private IDevStateScalar    C252_state_model;
  private ICommand           C252_reset_model;
  private String             C252_stt = new String();
  private StringScalar       C252_tra_mode_model;
  private String             C252_tra_mode;

  private SSAModePanel       C253Panel;
  private ModeComboBox       C253_Combo;
  private INumberScalar      C253_mode_model;
  private IDevStateScalar    C253_state_model;
  private ICommand           C253_reset_model;
  private String             C253_stt = new String();
  private StringScalar       C253_tra_mode_model;
  private String             C253_tra_mode;

  public MainPanel(String name) {
    this(name,false);
  }
  
  /**
   * Creates new form MainPanel
   */
  public MainPanel(String name,boolean runningFromShell) {
    
    // Init global variable
    
    this.runningFromShell = runningFromShell;

    errWin = new ErrorHistory();
    
    attList = new AttributeList();
    attList.addErrorListener(errWin);
    cmdList = new CommandList();
    cmdList.addErrorListener(errWin);
    cmdList.addErrorListener(ErrorPopup.getInstance());

    initComponents();
    
    // Handle windowClosing (Close from the system menu)
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        exitForm();
      }
    });

    // General KeyListener (for export mode)
    Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {
      public void eventDispatched(AWTEvent event) {
        KeyEvent ke = (KeyEvent) event;
        if (ke.getID() == KeyEvent.KEY_PRESSED && ke.getKeyCode() == KeyEvent.VK_F5) {
          if (ke.isShiftDown()) {
            toggleExpertMode();
          }
        }
      }
    }, AWTEvent.KEY_EVENT_MASK);

    // Splash window
    
    splash = new Splash();
    splash.setTitle("SRRF Wave Guide " + WGConst.APP_RELEASE);
    splash.setCopyright("(c) ESRF 2019");
    splash.setMaxProgress(100);
    splash.progress(0);
    int sp = 0;
    
    // Add mode panels
    enableWrite = false;
    
    // TRA1
    GridBagConstraints gbc = new GridBagConstraints();
    gbc.fill = GridBagConstraints.BOTH;
    gbc.gridx = 0;
    gbc.gridy = 1;
    ModePanel tra1Panel = new ModePanel("TRA1",attList);
    srrfPanel.add(tra1Panel,gbc);
    splash.progress(sp++);
    
    TRA1_Combo = new ModeComboBox();
    TRA1_Combo.addItem(new ModeComboItem("Interlocked",0));
    TRA1_Combo.addItem(new ModeComboItem("5 Cavities",1));
    TRA1_Combo.addItem(new ModeComboItem("10 Cavities",2));
    TRA1_Combo.addItem(new ModeComboItem("Dummy",3));
    TRA1_Combo.addItem(new ModeComboItem("HVPS Only",4));
    TRA1_Combo.addItem(new ModeComboItem("Anode Only",5));    
    //TRA1_Combo.addItem(new ModeComboItem("Test Stand Access",6));
    //TRA1_Combo.addItem(new ModeComboItem("Test Stand Powered",7));
    TRA1_Combo.addModeComboListener(this);
    tra1Panel.addModeCombo(TRA1_Combo);

    // TRA2
    ModePanel tra2Panel = new ModePanel("TRA2",attList);
    gbc.gridx = 1;
    srrfPanel.add(tra2Panel,gbc);
    splash.progress(sp++);
    
    TRA2_Combo = new ModeComboBox();
    TRA2_Combo.addItem(new ModeComboItem("Interlocked",0));
    TRA2_Combo.addItem(new ModeComboItem("5 Cavities",1));
    TRA2_Combo.addItem(new ModeComboItem("10 Cavities",2));
    TRA2_Combo.addItem(new ModeComboItem("Dummy",3));
    TRA2_Combo.addItem(new ModeComboItem("HVPS Only",4));
    TRA2_Combo.addItem(new ModeComboItem("Anode Only",5));   
    TRA2_Combo.addItem(new ModeComboItem("Test Stand Access",6));
    TRA2_Combo.addItem(new ModeComboItem("Test Stand Powered",7));
    TRA2_Combo.addModeComboListener(this);
    tra2Panel.addModeCombo(TRA2_Combo);

    gbc.gridy = 1;
    
    // SSA C25-1
    C251Panel = new SSAModePanel("C25-1",attList);
    gbc.gridx = 0;
    c25Panel.add(C251Panel,gbc);
    splash.progress(sp++);
    
    C251_Combo = new ModeComboBox();
    C251_Combo.addItem(new ModeComboItem("Cavity Op",1));
    C251_Combo.addItem(new ModeComboItem("Dummy",2));
    C251_Combo.addItem(new ModeComboItem("Cavity Cond",3));
    C251_Combo.addModeComboListener(this);
    C251Panel.addModeCombo(C251_Combo);

    // SSA C25-2
    C252Panel = new SSAModePanel("C25-2",attList);
    gbc.gridx = 1;
    c25Panel.add(C252Panel,gbc);
    splash.progress(sp++);
    
    C252_Combo = new ModeComboBox();
    C252_Combo.addItem(new ModeComboItem("Cavity Op",1));
    C252_Combo.addItem(new ModeComboItem("Dummy",2));
    C252_Combo.addItem(new ModeComboItem("Cavity Cond",3));
    C252_Combo.addModeComboListener(this);
    C252Panel.addModeCombo(C252_Combo);
    
    // SSA C25-3
    C253Panel = new SSAModePanel("C25-3",attList);
    gbc.gridx = 2;
    c25Panel.add(C253Panel,gbc);
    splash.progress(sp++);
    
    C253_Combo = new ModeComboBox();
    C253_Combo.addItem(new ModeComboItem("Cavity Op",1));
    C253_Combo.addItem(new ModeComboItem("Dummy",2));
    C253_Combo.addItem(new ModeComboItem("Cavity Cond",3));
    C253_Combo.addModeComboListener(this);
    C253Panel.addModeCombo(C253_Combo);
    
    // Tra Mode
    TRA1_tra_mode_model = initTraMode("srrf/tra/tra1/Transmitter_mode");
    TRA2_tra_mode_model = initTraMode("srrf/tra/tra2/Transmitter_mode");
    C251_tra_mode_model = initTraMode("srrf/ssa-wg/C25-1/Mode_Str");
    C252_tra_mode_model = initTraMode("srrf/ssa-wg/C25-2/Mode_Str");
    C253_tra_mode_model = initTraMode("srrf/ssa-wg/C25-3/Mode_Str");
    if(TRA1_tra_mode_model!=null) TRA1_tra_mode_model.refresh();
    if(TRA2_tra_mode_model!=null) TRA2_tra_mode_model.refresh();
    if(C251_tra_mode_model!=null) C251_tra_mode_model.refresh();
    if(C252_tra_mode_model!=null) C252_tra_mode_model.refresh();
    if(C253_tra_mode_model!=null) C253_tra_mode_model.refresh();
    
    // State models
    TRA1_state_model = initState("srrf/tra/tra1/State");
    splash.progress(sp++);
    TRA2_state_model = initState("srrf/tra/tra2/State");
    splash.progress(sp++);
    C251_state_model = initState("srrf/ssa-tra/c25-1/State");
    splash.progress(sp++);
    C252_state_model = initState("srrf/ssa-tra/c25-2/State");
    splash.progress(sp++);
    C253_state_model = initState("srrf/ssa-tra/c25-3/State");
    splash.progress(sp++);
        
    // Initialise ModeCombo
    TRA1_mode_model = initMode("srrf/cav-wg/1/SRRF1_mode",TRA1_Combo);
    splash.progress(sp++);
    TRA2_mode_model = initMode("srrf/cav-wg/1/SRRF2_mode",TRA2_Combo);
    splash.progress(sp++);
    C251_mode_model = initMode("srrf/ssa-wg/c25-1/mode",C251_Combo);
    splash.progress(sp++);
    C252_mode_model = initMode("srrf/ssa-wg/c25-2/mode",C252_Combo);
    splash.progress(sp++);
    C253_mode_model = initMode("srrf/ssa-wg/c25-3/mode",C253_Combo);
    splash.progress(sp++);
    
    sendSRRFModel = initCommand("srrf/cav-wg/1/Validate");
    splash.progress(sp++);
    TRA1_reset_model = initCommand("srrf/plc/tra1/Reset");
    splash.progress(sp++);
    TRA2_reset_model = initCommand("srrf/plc/tra2/Reset");
    splash.progress(sp++);
    sendC25Model = initCommand("srrf/ssa-wg/c25-1/Send");
    splash.progress(sp++);
    
    enableWrite = true;

    // Info panel viewer SRRF
    
    srrfInfoPanel = new JPanel();
    srrfInfoPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        
    plcRemoteViewer = new BooleanColorViewer("Remote");
    try {
      plcRemoteViewer.setModel((IBooleanScalar)attList.add("srrf/cav-wg/1/Remote"));
    } catch (ConnectionException e ) {}    
    srrfInfoPanel.add(plcRemoteViewer);
    splash.progress(sp++);
    
    transModeErrorViewer = new BooleanColorViewer("Mode error");
    transModeErrorViewer.setFalseBackground(Color.GREEN);
    transModeErrorViewer.setTrueBackground(Color.RED);
    try {
      transModeErrorViewer.setModel((IBooleanScalar) attList.add("srrf/cav-wg/1/ModeError"));
    } catch (ConnectionException e) {
    }
    srrfInfoPanel.add(transModeErrorViewer);
    splash.progress(sp++);
    
    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.gridwidth = 2;
    srrfPanel.add(srrfInfoPanel,gbc);
        
    // Info panel viewer C25
    
    c25InfoPanel = new JPanel();
    c25InfoPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    
    enable230VC25Viewer = new BooleanColorViewer("230V enable");
    enable230VC25Viewer.setTrueBackground(Color.GREEN);
    enable230VC25Viewer.setFalseBackground(Color.RED);
    try {
      enable230VC25Viewer.setModel((IBooleanScalar)attList.add("srrf/ssa-wg/c25-1/Enable_220V"));
    } catch (ConnectionException e ) {}
    gbc.gridy = 0;
    c25InfoPanel.add(enable230VC25Viewer);
    splash.progress(sp++);

    powerErrorViewer = new BooleanColorViewer("Power Error");
    powerErrorViewer.setTrueBackground(Color.RED);
    powerErrorViewer.setFalseBackground(Color.GREEN);
    try {
      powerErrorViewer.setModel((IBooleanScalar)attList.add("srrf/ssa-wg/c25-1/Power_Failure"));
    } catch (ConnectionException e ) {}
    c25InfoPanel.add(powerErrorViewer);
    splash.progress(sp++);
    
    remoteStatusViewer = new SimpleScalarViewer();
    remoteStatusViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    remoteStatusViewer.setMargin(new Insets(2, 20, 2, 20));
    remoteStatusViewer.setAlarmEnabled(false);
    remoteStatusViewer.setBackground(c25InfoPanel.getBackground());
    try {
      remoteStatusViewer.setModel((IStringScalar)attList.add("srrf/ssa-wg/c25-1/PLC_Mode"));
    } catch (ConnectionException e ) {}
    c25InfoPanel.add(remoteStatusViewer);
    splash.progress(sp++);

    c25InfoPanel.add(new JPanel(),gbc);

    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.gridwidth = 3;
    c25Panel.add(c25InfoPanel,gbc);
            
    // Loads the synoptic
    
    theSynoptic.setProgressListener(this);
    
    InputStream jdFileInStream = this.getClass().getResourceAsStream(WGConst.SYNOPTIC_FILE_NAME);

    if (jdFileInStream == null) {
      JOptionPane.showMessageDialog(
              null, "Failed to get the inputStream for the synoptic file resource : " + WGConst.SYNOPTIC_FILE_NAME + " \n\n",
              "Resource error",
              JOptionPane.ERROR_MESSAGE);
      exitForm();
    }

    InputStreamReader inStrReader = new InputStreamReader(jdFileInStream);
    try {
      theSynoptic.setErrorHistoryWindow(errWin);
      theSynoptic.setToolTipMode(TangoSynopticHandler.TOOL_TIP_NAME);
      theSynoptic.setAutoZoom(true);
      theSynoptic.loadSynopticFromStream(inStrReader);
    } catch (IOException ioex) {
      JOptionPane.showMessageDialog(
              null, "Cannot find load the synoptic input stream reader.\n" + " Application will abort ...\n" + ioex,
              "Resource access failed",
              JOptionPane.ERROR_MESSAGE);
      exitForm();
    }
    
    // Create the WG network object
    // Compute path of the RF inside the wave guide network
    wgNetwork = new WGNetwork(theSynoptic);
    new Thread() {
      public void run() {
        while(true) {
          wgNetwork.computeWGStates();
          try {
            Thread.sleep(2000);
          } catch( InterruptedException e) {}
        }
      }
    }.start();
    
    attList.setRefreshInterval(1000);
    attList.startRefresher();
    
    splash.setVisible(false);

    setTitle("SR Wave Guide Switches " + WGConst.APP_RELEASE);
    ATKGraphicsUtils.centerFrameOnScreen(this);
    setVisible(true);
    
  }
  
  private ICommand initCommand(String cmdName) {
    
    ICommand cmd=null;
    
    try {
      cmd = CommandFactory.getInstance().getCommand(cmdName);
      cmd.addErrorListener(ErrorPopup.getInstance());
    } catch (DevFailed e1) {
      splash.setVisible(false);
      ErrorPane.showErrorMessage(this,cmdName,e1);
    } catch (ConnectionException e2) {
      splash.setVisible(false);
      ErrorPane.showErrorMessage(this,cmdName,e2);
    }
    
    return cmd;

  }

  private StringScalar initTraMode(String attName) {
    
    StringScalar model=null;
    
    try {
      model = (StringScalar) attList.add(attName);
      model.addStringScalarListener(this);
    } catch (ConnectionException e2) {
      splash.setVisible(false);
      ErrorPane.showErrorMessage(this,attName,e2);
    }
    
    return model;
   
  }
  
  private INumberScalar initMode(String attName,ModeComboBox combo) {

    INumberScalar model=null;
    
    try {
      model = (INumberScalar) AttributeFactory.getInstance().getAttribute(attName);
      model.addErrorListener(ErrorPopup.getInstance());
      model.refresh();
      combo.setMode((int)model.getNumberScalarValue());
    } catch (DevFailed e1) {
      splash.setVisible(false);
      ErrorPane.showErrorMessage(this,attName,e1);
    } catch (ConnectionException e2) {
      splash.setVisible(false);
      ErrorPane.showErrorMessage(this,attName,e2);
    }
    
    return model;
    
  }

  private IDevStateScalar initState(String attName) {

    IDevStateScalar model=null;

    try {
      model = (IDevStateScalar)attList.add(attName);
      model.addDevStateScalarListener(this);
      model.refresh();
    } catch (ConnectionException e ) {}
    
    return model;

  }

  public void progress(double p) {
    splash.progress(50+(int)(p*50.0));
  }
  
  
  public void stringScalarChange(StringScalarEvent evt) {
    
    Object src = evt.getSource();
    
    if(src==TRA1_tra_mode_model) {
      TRA1_tra_mode = evt.getValue();
    } else if(src==TRA2_tra_mode_model) {
      TRA2_tra_mode = evt.getValue();
    } else if(src==C251_tra_mode_model) {
      C251_tra_mode = evt.getValue();
    } else if(src==C252_tra_mode_model) {
      C252_tra_mode = evt.getValue();
    } else if(src==C253_tra_mode_model) {
      C253_tra_mode = evt.getValue();
    }
    
  }

  /**
   * Exit the application
   */
  public void exitForm() {

    if (runningFromShell) {

      System.exit(0);

    } else {

      clearModel();
      setVisible(false);
      dispose();

    }

  }

  /**
   * Clear all connections to attributes and commands
   */
  private void clearModel() {

    theSynoptic.clearSynopticFileModel();
    
    TRA1_state_model.removeDevStateScalarListener(this);
    TRA2_state_model.removeDevStateScalarListener(this);
    C251_state_model.removeDevStateScalarListener(this);
    C252_state_model.removeDevStateScalarListener(this);
    C253_state_model.removeDevStateScalarListener(this);
    
    TRA1_tra_mode_model.removeStringScalarListener(this);
    TRA2_tra_mode_model.removeStringScalarListener(this);
    C251_tra_mode_model.removeStringScalarListener(this);
    C252_tra_mode_model.removeStringScalarListener(this);
    C253_tra_mode_model.removeStringScalarListener(this);
    
    TRA1_mode_model.removeErrorListener(ErrorPopup.getInstance());
    TRA2_mode_model.removeErrorListener(ErrorPopup.getInstance());
    C251_mode_model.removeErrorListener(ErrorPopup.getInstance());
    C252_mode_model.removeErrorListener(ErrorPopup.getInstance());
    C253_mode_model.removeErrorListener(ErrorPopup.getInstance());
    
    TRA1_reset_model.removeErrorListener(ErrorPopup.getInstance());
    TRA2_reset_model.removeErrorListener(ErrorPopup.getInstance());
    C251_reset_model.removeErrorListener(ErrorPopup.getInstance());
    C252_reset_model.removeErrorListener(ErrorPopup.getInstance());
    C253_reset_model.removeErrorListener(ErrorPopup.getInstance());
    
    sendSRRFModel.removeErrorListener(ErrorPopup.getInstance());
    sendC25Model.removeErrorListener(ErrorPopup.getInstance());
    
    tra1Panel.clearModel();
    tra2Panel.clearModel();
    C251Panel.clearModel();
    C252Panel.clearModel();
    C253Panel.clearModel();
    
    plcRemoteViewer.clearModel();
    transModeErrorViewer.clearModel();

    //enable230VC25Viewer.clearModel();
    //powerErrorViewer.clearModel();
    //remoteStatusViewer.clearModel();
               
    attList.removeErrorListener(errWin);
    attList.stopRefresher();
    attList.clear();
    
    cmdList.removeErrorListener(errWin);
    cmdList.removeErrorListener(ErrorPopup.getInstance());
    cmdList.clear();
    
  }
    
  public void modeChange(ModeComboBox src) {

    traModeChange();

  }

  private void traModeChange() {

    TRA1_Combo.enableAll();
    TRA2_Combo.enableAll();

    int tra1Mode = TRA1_Combo.getMode();
    int tra2Mode = TRA2_Combo.getMode();
    int c251Mode = C251_Combo.getMode();
    int c252Mode = C252_Combo.getMode();
    int c253Mode = C253_Combo.getMode();
    
    // Disable menu item to avoid invalid WG configuration
    switch(tra1Mode) {
      case 1: // 5 Cav
        TRA2_Combo.disableItem(2);
        break;
      case 2: // 10 cav
        TRA2_Combo.disableItem(1);
        TRA2_Combo.disableItem(2);
        break;
    }

    switch(tra2Mode) {
      case 1: // 5 Cav
        TRA1_Combo.disableItem(2);
        break;
      case 2: // 10 cav
        TRA1_Combo.disableItem(1);
        TRA1_Combo.disableItem(2);
        break;
    }

    // Write mode to the DS
    if( enableWrite ) {
      TRA1_mode_model.setValue((double)tra1Mode);
      TRA2_mode_model.setValue((double)tra2Mode);
      C251_mode_model.setValue((double)c251Mode);
      C252_mode_model.setValue((double)c252Mode);
      C253_mode_model.setValue((double)c253Mode);
    }

  }
  
  public void devStateScalarChange(DevStateScalarEvent evt) {

    Object src = evt.getSource();
    String state = evt.getValue();

    if (src == TRA1_state_model) {
      TRA1_stt = state;
    } else if (src == TRA2_state_model) {
      TRA2_stt = state;
    } else if (src == C251_state_model ) {
      C251_stt = state;
    } else if (src == C252_state_model ) {
      C252_stt = state;
    } else if (src == C253_state_model ) {
      C253_stt = state;
    }
    

    enablePanelItem();

  }
  
  private void SendToPLC() {

    //Check TRA2 on test stand powered
    if( TRA2_Combo.getMode()==7 ) {

      if( JOptionPane.showConfirmDialog(this,"Make sure that the search has been made in the test stand,\n"
                                             +"otherwise the beam will be killed.\n"
                                             +"Do you really want to send the configuration to the PLC ?",
                                             "Save confirmation",JOptionPane.YES_NO_OPTION)==JOptionPane.NO_OPTION) {
        return;
      }

    }
        
    boolean okTRA1 = (TRA1_Combo.getMode()==2 && TRA1_tra_mode.equals("CAVITIES_5")) ||
                     (TRA1_Combo.getMode()==1 && TRA1_tra_mode.equals("CAVITIES_10")) ||
                     (!TRA1_tra_mode.equals("CAVITIES_10") && !TRA1_tra_mode.equals("CAVITIES_5"));
    
    if( !okTRA1 ) {
    
        if( JOptionPane.showConfirmDialog(this,"You are going to change the configuration of TRA1 which is connected to SR.\n"
                   +"Do you really want to send the new configuration to the PLC ?",
               "Confirmation",JOptionPane.YES_NO_OPTION)==JOptionPane.NO_OPTION)
        return;
        
    }

    boolean okTRA2 = (TRA2_Combo.getMode()==2 && TRA2_tra_mode.equals("CAVITIES_10")) ||
                     (TRA2_Combo.getMode()==1 && TRA2_tra_mode.equals("CAVITIES_5")) ||
                     (!TRA2_tra_mode.equals("CAVITIES_10") && !TRA2_tra_mode.equals("CAVITIES_5"));
    
    if( !okTRA2 ) {
    
        if( JOptionPane.showConfirmDialog(this,"You are going to change the configuration of TRA2 which is connected to SR.\n"
                   +"Do you really want to send the new configuration to the PLC ?",
               "Confirmation",JOptionPane.YES_NO_OPTION)==JOptionPane.NO_OPTION)
        return;
        
    }
    
    sendSRRFModel.execute();

    // Reset transmitter
    try {
      Thread.sleep(1000);
    } catch(Exception e) {}

    TRA1_reset_model.execute();
    TRA2_reset_model.execute();

  }

  public void toggleExpertMode() {

    if( expertMode ) {

      expertMode=false;
      //expertModeViewer.setToggleBackground(Color.WHITE);
      
    } else {

      if( JOptionPane.showConfirmDialog(null,"You will have access to all menus.\n"
                                             +"Do you want to switch to expert mode ?",
                                             "Expert Mode Confirmation",JOptionPane.YES_NO_OPTION)==JOptionPane.NO_OPTION) {
        return;
      }

      expertMode=true;
      //expertModeViewer.setToggleBackground(Color.ORANGE);

    }
    enablePanelItem();

  }

  public void stateChange(AttributeStateEvent e)
  {
  }

  public void errorChange(ErrorEvent evt)
  {

    Object src = evt.getSource();

    if( src==TRA1_state_model ) {
      TRA1_stt = IDevice.UNKNOWN;
    } else if ( src==TRA2_state_model ) {
      TRA2_stt = IDevice.UNKNOWN;
    } else if ( src==C251_state_model ) {
      C251_stt = IDevice.UNKNOWN;
    } else if ( src==C252_state_model ) {
      C252_stt = IDevice.UNKNOWN;
    } else if ( src==C253_state_model ) {
      C253_stt = IDevice.UNKNOWN;
    }

    enablePanelItem();

  }

  private void enablePanelItem() {

    sendSRRFButton.setEnabled(true);

    // Check transmitter mode
    if (!expertMode) {

      if (TRA1_stt.equalsIgnoreCase(IDevice.ON) ||
          TRA1_stt.equalsIgnoreCase(IDevice.ALARM) ) {
        TRA1_Combo.setEnabled(false);
      } else {
        TRA1_Combo.setEnabled(true);
      }

      if (TRA2_stt.equalsIgnoreCase(IDevice.ON) ||
          TRA2_stt.equalsIgnoreCase(IDevice.ALARM) ) {
        TRA2_Combo.setEnabled(false);
      } else {
        TRA2_Combo.setEnabled(true);
      }

      if (C251_stt.equalsIgnoreCase(IDevice.ON) ||
          C251_stt.equalsIgnoreCase(IDevice.ALARM)) {
        C251_Combo.setEnabled(false);
      } else {
        C251_Combo.setEnabled(true);
      }
      
      if (C252_stt.equalsIgnoreCase(IDevice.ON) ||
          C252_stt.equalsIgnoreCase(IDevice.ALARM)) {
        C252_Combo.setEnabled(false);
      } else {
        C252_Combo.setEnabled(true);
      }
      
      if (C253_stt.equalsIgnoreCase(IDevice.ON) ||
          C253_stt.equalsIgnoreCase(IDevice.ALARM)) {
        C253_Combo.setEnabled(false);
      } else {
        C253_Combo.setEnabled(true);
      }
              
    } else {

      // Expert mode
      TRA1_Combo.setEnabled(true);
      TRA2_Combo.setEnabled(true);
      C251_Combo.setEnabled(true);
      C252_Combo.setEnabled(true);
      C253_Combo.setEnabled(true);

    }

  }

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {
    java.awt.GridBagConstraints gridBagConstraints;

    synopticPanel = new javax.swing.JPanel();
    theSynoptic = new fr.esrf.tangoatk.widget.jdraw.SynopticFileViewer();
    downPanel = new javax.swing.JPanel();
    settingPanel = new javax.swing.JPanel();
    srrfGlobalPanel = new javax.swing.JPanel();
    srrfPanel = new javax.swing.JPanel();
    globalSRRFPanel = new javax.swing.JPanel();
    sendSRRFButton = new javax.swing.JButton();
    dummyPanel = new javax.swing.JPanel();
    c25GlobalPanel = new javax.swing.JPanel();
    c25Panel = new javax.swing.JPanel();
    globalC251Panel = new javax.swing.JPanel();
    sendC25Button = new javax.swing.JButton();
    jMenuBar1 = new javax.swing.JMenuBar();
    fileMenu = new javax.swing.JMenu();
    exitMenuItem = new javax.swing.JMenuItem();
    viewMenu = new javax.swing.JMenu();
    viewErrorMenuItem = new javax.swing.JMenuItem();
    viewDiagMenuItem = new javax.swing.JMenuItem();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

    synopticPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
    synopticPanel.setLayout(new java.awt.BorderLayout());

    theSynoptic.setAutoZoom(true);
    synopticPanel.add(theSynoptic, java.awt.BorderLayout.CENTER);

    getContentPane().add(synopticPanel, java.awt.BorderLayout.CENTER);

    downPanel.setLayout(new java.awt.BorderLayout());

    settingPanel.setLayout(new java.awt.GridBagLayout());

    srrfGlobalPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("SRRF"));
    srrfGlobalPanel.setLayout(new java.awt.GridBagLayout());

    srrfPanel.setLayout(new java.awt.GridBagLayout());
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    srrfGlobalPanel.add(srrfPanel, gridBagConstraints);

    globalSRRFPanel.setLayout(new java.awt.GridBagLayout());

    sendSRRFButton.setText("Send to PLC (SRRF)");
    sendSRRFButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        sendSRRFButtonActionPerformed(evt);
      }
    });
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
    globalSRRFPanel.add(sendSRRFButton, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.weightx = 1.0;
    srrfGlobalPanel.add(globalSRRFPanel, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    settingPanel.add(srrfGlobalPanel, gridBagConstraints);
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.weightx = 1.0;
    settingPanel.add(dummyPanel, gridBagConstraints);

    c25GlobalPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("C25"));
    c25GlobalPanel.setLayout(new java.awt.GridBagLayout());

    c25Panel.setLayout(new java.awt.GridBagLayout());
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    c25GlobalPanel.add(c25Panel, gridBagConstraints);

    globalC251Panel.setLayout(new java.awt.GridBagLayout());

    sendC25Button.setText("Send to PLC (C25)");
    sendC25Button.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        sendC25ButtonActionPerformed(evt);
      }
    });
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 100;
    globalC251Panel.add(sendC25Button, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
    c25GlobalPanel.add(globalC251Panel, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    settingPanel.add(c25GlobalPanel, gridBagConstraints);

    downPanel.add(settingPanel, java.awt.BorderLayout.CENTER);

    getContentPane().add(downPanel, java.awt.BorderLayout.SOUTH);

    fileMenu.setText("File");

    exitMenuItem.setText("Exit");
    exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        exitMenuItemActionPerformed(evt);
      }
    });
    fileMenu.add(exitMenuItem);

    jMenuBar1.add(fileMenu);

    viewMenu.setText("View");

    viewErrorMenuItem.setText("Errors ...");
    viewErrorMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        viewErrorMenuItemActionPerformed(evt);
      }
    });
    viewMenu.add(viewErrorMenuItem);

    viewDiagMenuItem.setText("Diagnostics ...");
    viewDiagMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        viewDiagMenuItemActionPerformed(evt);
      }
    });
    viewMenu.add(viewDiagMenuItem);

    jMenuBar1.add(viewMenu);

    setJMenuBar(jMenuBar1);

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
    // TODO add your handling code here:
    exitForm();
  }//GEN-LAST:event_exitMenuItemActionPerformed

  private void viewErrorMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewErrorMenuItemActionPerformed
    // TODO add your handling code here:
    ATKGraphicsUtils.centerFrameOnScreen(errWin);
    errWin.setVisible(true);
  }//GEN-LAST:event_viewErrorMenuItemActionPerformed

  private void viewDiagMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewDiagMenuItemActionPerformed
    // TODO add your handling code here:
    ATKDiagnostic.showDiagnostic();
  }//GEN-LAST:event_viewDiagMenuItemActionPerformed

  private void sendSRRFButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendSRRFButtonActionPerformed
    // TODO add your handling code here:
    SendToPLC();
  }//GEN-LAST:event_sendSRRFButtonActionPerformed

  private void sendC25ButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendC25ButtonActionPerformed
    // TODO add your handling code here:
    
    boolean okC251 = (C251_Combo.getMode()==1 && C251_tra_mode.equals("CAVITY")) ||
                     (!C251_tra_mode.equals("CAVITY"));
    
    if( !okC251 ) {
    
        if( JOptionPane.showConfirmDialog(this,"You are going to change the configuration of C25-1 which is connected to SR.\n"
                   +"Do you really want to send the new configuration to the PLC ?",
               "Confirmation",JOptionPane.YES_NO_OPTION)==JOptionPane.NO_OPTION)
        return;
        
    }
      
    boolean okC252 = (C252_Combo.getMode()==1 && C252_tra_mode.equals("CAVITY")) ||
                     (!C252_tra_mode.equals("CAVITY"));
    
    if( !okC252 ) {
    
        if( JOptionPane.showConfirmDialog(this,"You are going to change the configuration of C25-2 which is connected to SR.\n"
                   +"Do you really want to send the new configuration to the PLC ?",
               "Confirmation",JOptionPane.YES_NO_OPTION)==JOptionPane.NO_OPTION)
        return;
        
    }
    
    boolean okC253 = (C253_Combo.getMode()==1 && C253_tra_mode.equals("CAVITY")) ||
                     (!C253_tra_mode.equals("CAVITY"));
    
    if( !okC253 ) {
    
        if( JOptionPane.showConfirmDialog(this,"You are going to change the configuration of C25-3 which is connected to SR.\n"
                   +"Do you really want to send the new configuration to the PLC ?",
               "Confirmation",JOptionPane.YES_NO_OPTION)==JOptionPane.NO_OPTION)
        return;
        
    }
    
    sendC25Model.execute();
    
  }//GEN-LAST:event_sendC25ButtonActionPerformed

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    new MainPanel("",true);
  }
  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JPanel c25GlobalPanel;
  private javax.swing.JPanel c25Panel;
  private javax.swing.JPanel downPanel;
  private javax.swing.JPanel dummyPanel;
  private javax.swing.JMenuItem exitMenuItem;
  private javax.swing.JMenu fileMenu;
  private javax.swing.JPanel globalC251Panel;
  private javax.swing.JPanel globalSRRFPanel;
  private javax.swing.JMenuBar jMenuBar1;
  private javax.swing.JButton sendC25Button;
  private javax.swing.JButton sendSRRFButton;
  private javax.swing.JPanel settingPanel;
  private javax.swing.JPanel srrfGlobalPanel;
  private javax.swing.JPanel srrfPanel;
  private javax.swing.JPanel synopticPanel;
  private fr.esrf.tangoatk.widget.jdraw.SynopticFileViewer theSynoptic;
  private javax.swing.JMenuItem viewDiagMenuItem;
  private javax.swing.JMenuItem viewErrorMenuItem;
  private javax.swing.JMenu viewMenu;
  // End of variables declaration//GEN-END:variables
}
