package jrfwaveguide;

import fr.esrf.tangoatk.widget.util.jdraw.JDObject;
import fr.esrf.tangoatk.widget.util.jdraw.JDrawEditor;

import java.awt.*;
import java.util.Vector;


class NetWorkItem {

  NetWorkItem() {
    wg = null;
    wgSwitch = null;
    next1 = null;
    next2 = null;
  }

  JDObject wg;           // The wave guide
  JDObject wgSwitch;     // Wage guide switch (at the end of the wg)

  NetWorkItem next1;     // The next wage guide (wg_switch position 1)
  NetWorkItem next2;     // The next waveguide  (wg_switch position 2)

}

/**
 * A class that compute RF path in the wage guide network
 */
public class WGNetwork {

  JDrawEditor jde;

  // Starting node
  NetWorkItem TRA1;
  NetWorkItem TRA2;
  NetWorkItem C251;
  NetWorkItem C252;
  NetWorkItem C253;

  WGNetwork(JDrawEditor jde) {

    this.jde = jde;

    // Construct the network wave guide

    NetWorkItem S14_CAV15 = new NetWorkItem();
    S14_CAV15.wg = getObjectByName("w_S14_CAV15");
    
    NetWorkItem T_S141 = new NetWorkItem();
    T_S141.wg = getObjectByName("w_T_S141");
    T_S141.wgSwitch = getObjectByName("srrf/cav-wg/1/S14");
    T_S141.next1 = S14_CAV15;

    NetWorkItem S132_S142 = new NetWorkItem();
    S132_S142.wg = getObjectByName("w_S132_S142");
    S132_S142.wgSwitch = getObjectByName("srrf/cav-wg/1/S14");
    S132_S142.next2 = S14_CAV15;

    NetWorkItem S24_CAV510 = new NetWorkItem();
    S24_CAV510.wg = getObjectByName("w_S24_CAV510");

    NetWorkItem T_S242 = new NetWorkItem();
    T_S242.wg = getObjectByName("w_T_S242");
    T_S242.wgSwitch = getObjectByName("srrf/cav-wg/1/S24");
    T_S242.next2 = S24_CAV510;

    NetWorkItem S131_T = new NetWorkItem();
    S131_T.wg = getObjectByName("w_S131_T");
    S131_T.next1 = T_S141;
    S131_T.next2 = T_S242;

    NetWorkItem S12_S13 = new NetWorkItem();
    S12_S13.wg = getObjectByName("w_S12_S13");
    S12_S13.wgSwitch = getObjectByName("srrf/cav-wg/1/S13");
    S12_S13.next1 = S131_T;
    S12_S13.next2 = S132_S142;

    NetWorkItem S212_S122 = new NetWorkItem();
    S212_S122.wg = getObjectByName("w_S212_S122");
    S212_S122.wgSwitch = getObjectByName("srrf/cav-wg/1/S12");
    S212_S122.next2 = S12_S13;

    NetWorkItem S251_TS = new NetWorkItem();
    S251_TS.wg = getObjectByName("w_S251_TS");

    NetWorkItem S252_SC = new NetWorkItem();
    S252_SC.wg = getObjectByName("w_S252_SC");

    NetWorkItem S232_S25 = new NetWorkItem();
    S232_S25.wg = getObjectByName("w_S232_S25");
    S232_S25.wgSwitch = getObjectByName("srrf/cav-wg/1/S25");
    S232_S25.next1 = S251_TS;
    S232_S25.next2 = S252_SC;

    NetWorkItem S231_S241 = new NetWorkItem();
    S231_S241.wg = getObjectByName("w_S231_S241");
    S231_S241.wgSwitch = getObjectByName("srrf/cav-wg/1/S24");
    S231_S241.next1 = S24_CAV510;

    NetWorkItem S222_S23 = new NetWorkItem();
    S222_S23.wg = getObjectByName("w_S222_S23");
    S222_S23.wgSwitch = getObjectByName("srrf/cav-wg/1/S23");
    S222_S23.next1 = S231_S241;
    S222_S23.next2 = S232_S25;

    NetWorkItem S221_D = new NetWorkItem();
    S221_D.wg = getObjectByName("w_S221_D");

    NetWorkItem S211_S22 = new NetWorkItem();
    S211_S22.wg = getObjectByName("w_S211_S22");
    S211_S22.wgSwitch = getObjectByName("srrf/cav-wg/1/S22");
    S211_S22.next1 = S221_D;
    S211_S22.next2 = S222_S23;

    TRA2 = new NetWorkItem();
    TRA2.wg = getObjectByName("w_tra2_S21");
    TRA2.wgSwitch = getObjectByName("srrf/cav-wg/1/S21");
    TRA2.next1 = S211_S22;
    TRA2.next2 = S212_S122;

    NetWorkItem S112_D = new NetWorkItem();
    S112_D.wg = getObjectByName("w_S112_D");

    NetWorkItem S111_S121 = new NetWorkItem();
    S111_S121.wg = getObjectByName("w_S111_S121");
    S111_S121.wgSwitch = getObjectByName("srrf/cav-wg/1/S12");
    S111_S121.next1 = S12_S13;
    
    TRA1 = new NetWorkItem();
    TRA1.wg = getObjectByName("w_tra1_S11");
    TRA1.wgSwitch = getObjectByName("srrf/cav-wg/1/S11");
    TRA1.next1 = S111_S121;
    TRA1.next2 = S112_D;
    
    // C25-1
    NetWorkItem C251_S2_CAV = new NetWorkItem();
    C251_S2_CAV.wg = getObjectByName("w_C251_S2_CAV");
    
    NetWorkItem C251_S11_S21 = new NetWorkItem();
    C251_S11_S21.wg = getObjectByName("w_C251_S11_S21");
    C251_S11_S21.wgSwitch = getObjectByName("srrf/ssa-wg/c25-1/S2_Position");
    C251_S11_S21.next1 = C251_S2_CAV;

    NetWorkItem C251_S1_D = new NetWorkItem();
    C251_S1_D.wg = getObjectByName("w_C251_S1_D");

    C251 = new NetWorkItem();
    C251.wg = getObjectByName("w_C251_TRA_S1");
    C251.wgSwitch = getObjectByName("srrf/ssa-wg/c25-1/S1_Position");
    C251.next1 = C251_S11_S21;
    C251.next2 = C251_S1_D;

    // C25-2
    NetWorkItem C252_S2_CAV = new NetWorkItem();
    C252_S2_CAV.wg = getObjectByName("w_C252_S2_CAV");
    
    NetWorkItem C252_S11_S21 = new NetWorkItem();
    C252_S11_S21.wg = getObjectByName("w_C252_S11_S21");
    C252_S11_S21.wgSwitch = getObjectByName("srrf/ssa-wg/c25-2/S2_Position");
    C252_S11_S21.next1 = C252_S2_CAV;

    NetWorkItem C252_S1_D = new NetWorkItem();
    C252_S1_D.wg = getObjectByName("w_C252_S1_D");

    C252 = new NetWorkItem();
    C252.wg = getObjectByName("w_C252_TRA_S1");
    C252.wgSwitch = getObjectByName("srrf/ssa-wg/c25-2/S1_Position");
    C252.next1 = C252_S11_S21;
    C252.next2 = C252_S1_D;
        
    // C25-3
    NetWorkItem C253_S2_CAV = new NetWorkItem();
    C253_S2_CAV.wg = getObjectByName("w_C253_S2_CAV");
    
    NetWorkItem C253_S11_S21 = new NetWorkItem();
    C253_S11_S21.wg = getObjectByName("w_C253_S11_S21");
    C253_S11_S21.wgSwitch = getObjectByName("srrf/ssa-wg/c25-3/S2_Position");
    C253_S11_S21.next1 = C253_S2_CAV;

    NetWorkItem C253_S1_D = new NetWorkItem();
    C253_S1_D.wg = getObjectByName("w_C253_S1_D");

    C253 = new NetWorkItem();
    C253.wg = getObjectByName("w_C253_TRA_S1");
    C253.wgSwitch = getObjectByName("srrf/ssa-wg/c25-3/S1_Position");
    C253.next1 = C253_S11_S21;
    C253.next2 = C253_S1_D;
    
  }

  private JDObject getObjectByName(String name) {

    Vector v = jde.getObjectsByName(name,false);

    if( v.size()==0 ) {
      System.out.println("Warning: object "+name+" not found");
      return null;
    }

    if(v.size()>1) {
      System.out.println("Warning: several objects have the same name: "+name);
    }

    return (JDObject)v.get(0);

  }

  public void computeWGStates() {

    resetNetwork();
    browseNetwork(TRA1);
    browseNetwork(TRA2);
    browseNetwork(C251);
    browseNetwork(C252);
    browseNetwork(C253);
    jde.repaint();

  }

  private void resetNetwork() {

    for(int i=0;i<jde.getObjectNumber();i++) {
      JDObject o = jde.getObjectAt(i);
      if(o.getName().startsWith("w_")) {
        o.setBackground(Color.WHITE);
      }
    }

  }

  private void browseNetwork(NetWorkItem n) {

    n.wg.setBackground(Color.GREEN);

    if(n.wgSwitch!=null) {

      int v = n.wgSwitch.getValue();
      if(v==1 && n.next1!=null) browseNetwork(n.next1);
      if(v==2 && n.next2!=null) browseNetwork(n.next2);

    } else {

      // Special case for the magic tee
      if(n.next1!=null && n.next2!=null) {
        browseNetwork(n.next1);
        browseNetwork(n.next2);
      }

    }
    
  }

}
