package jrfwaveguide;

import fr.esrf.Tango.DevFailed;
import fr.esrf.tangoatk.core.*;
import fr.esrf.tangoatk.core.attribute.AttributeFactory;
import fr.esrf.tangoatk.core.command.CommandFactory;
import fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.ErrorPopup;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Panel for transmitter and wave guide mode
 */
public class ModePanel extends JPanel implements ModeComboListener, ActionListener, IDevStateScalarListener {

  public final static Font borderFont = new Font("Dialog",Font.BOLD,12);

  SimpleScalarViewer TRA0_state;
  SimpleScalarViewer TRA0_mode;
  SimpleScalarViewer TRA0_wg_mode;
  ModeComboBox       TRA0_Combo;
  INumberScalar      TRA0_mode_model;

  SimpleScalarViewer TRA1_state;
  SimpleScalarViewer TRA1_mode;
  SimpleScalarViewer TRA1_wg_mode;
  ModeComboBox       TRA1_Combo;
  INumberScalar      TRA1_mode_model;
  IDevStateScalar    TRA1_state_model;
  String             TRA1_stt = new String();

  SimpleScalarViewer TRA2_state;
  SimpleScalarViewer TRA2_mode;
  SimpleScalarViewer TRA2_wg_mode;
  ModeComboBox       TRA2_Combo;
  INumberScalar      TRA2_mode_model;
  IDevStateScalar    TRA2_state_model;
  String             TRA2_stt = new String();

  SimpleScalarViewer TRA3_state;
  SimpleScalarViewer TRA3_mode;
  SimpleScalarViewer TRA3_wg_mode;
  ModeComboBox       TRA3_Combo;
  INumberScalar      TRA3_mode_model;
  IDevStateScalar    TRA3_state_model;
  String             TRA3_stt = new String();

  BooleanColorViewer plcRemoteViewer;
  BooleanColorViewer enable230VViewer;
  BooleanColorViewer transModeErrorViewer;
  BooleanColorViewer wgModeErrorViewer;
  BooleanColorViewer manualModeViewer;
  BooleanColorViewer expertModeViewer;

  JButton            sendBtn;
  ICommand           sendModel;
  ICommand           resetTRA1Model;
  ICommand           resetTRA2Model;
  ICommand           resetTRA3Model;

  boolean            enableWrite;
  boolean            expertMode = false;

  ModePanel(AttributeList attList) {

    setLayout(null);
    setPreferredSize(new Dimension(0,205));

    // Global -------------------------------------------------------------------------
    JPanel GlobalPanel = new JPanel();
    GlobalPanel.setLayout(null);
    GlobalPanel.setBorder( BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Global",
                         TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION,
                         borderFont, Color.BLACK) );
    GlobalPanel.setBounds(5,150,970,50);
    add(GlobalPanel);

    plcRemoteViewer = new BooleanColorViewer("PLC Remote");
    plcRemoteViewer.setBounds(10,20,120,25);
    try {
      plcRemoteViewer.setModel((IBooleanScalar)attList.add("sr/rf-switch/manager/PLC_remote"));
    } catch (ConnectionException e ) {}
    GlobalPanel.add(plcRemoteViewer);

    enable230VViewer = new BooleanColorViewer("230V enable");
    enable230VViewer.setBounds(130,20,120,25);
    try {
      enable230VViewer.setModel((IBooleanScalar)attList.add("sr/rf-switch/manager/Enable_230V"));
    } catch (ConnectionException e ) {}
    GlobalPanel.add(enable230VViewer);


    transModeErrorViewer = new BooleanColorViewer("Transmitter mode error");
    transModeErrorViewer.setBounds(250, 20, 170, 25);
    transModeErrorViewer.setFalseBackground(Color.GREEN);
    transModeErrorViewer.setTrueBackground(Color.RED);
    try {
      transModeErrorViewer.setModel((IBooleanScalar) attList.add("sr/rf-switch/manager/Trans_mode_error"));
    } catch (ConnectionException e) {
    }
    GlobalPanel.add(transModeErrorViewer);

    wgModeErrorViewer = new BooleanColorViewer("W.G. mode error");
    wgModeErrorViewer.setBounds(420, 20, 150, 25);
    wgModeErrorViewer.setFalseBackground(Color.GREEN);
    wgModeErrorViewer.setTrueBackground(Color.RED);
    try {
      wgModeErrorViewer.setModel((IBooleanScalar) attList.add("sr/rf-switch/manager/WG_mode_error"));
    } catch (ConnectionException e) {
    }
    GlobalPanel.add(wgModeErrorViewer);

    manualModeViewer = new BooleanColorViewer("Manual mode");
    manualModeViewer.setBounds(570,20,120,25);
    try {
      manualModeViewer.setModel((IBooleanScalar)attList.add("sr/rf-switch/manager/Manual_mode"));
    } catch (ConnectionException e ) {}
    GlobalPanel.add(manualModeViewer);

    expertModeViewer = new BooleanColorViewer("Expert mode");
    expertModeViewer.setToggleBackground(Color.WHITE);
    expertModeViewer.setBounds(690,20,120,25);
    GlobalPanel.add(expertModeViewer);

    sendBtn = new JButton("Send to PLC");
    sendBtn.setBounds(810,15,150,25);
    sendBtn.addActionListener(this);
    GlobalPanel.add(sendBtn);
    try {
      sendModel = CommandFactory.getInstance().getCommand("sr/rf-switch/manager/send");
      sendModel.addErrorListener(ErrorPopup.getInstance());
    } catch (DevFailed e1) {
      ErrorPane.showErrorMessage(this,"sr/rf-switch/manager/send",e1);
    } catch (ConnectionException e2) {
      ErrorPane.showErrorMessage(this,"sr/rf-switch/manager/send",e2);
    }

    try {
      resetTRA1Model = CommandFactory.getInstance().getCommand("taco:sr/rf-plc/tra1/DevReset");
      resetTRA1Model.addErrorListener(ErrorPopup.getInstance());
    } catch (DevFailed e1) {
      ErrorPane.showErrorMessage(this,"taco:sr/rf-plc/tra1/reset",e1);
    } catch (ConnectionException e2) {
      ErrorPane.showErrorMessage(this,"taco:sr/rf-plc/tra1/reset",e2);
    }

    try {
      resetTRA2Model = CommandFactory.getInstance().getCommand("taco:sr/rf-plc/tra2/DevReset");
      resetTRA2Model.addErrorListener(ErrorPopup.getInstance());
    } catch (DevFailed e1) {
      ErrorPane.showErrorMessage(this,"taco:sr/rf-plc/tra2/reset",e1);
    } catch (ConnectionException e2) {
      ErrorPane.showErrorMessage(this,"taco:sr/rf-plc/tra2/reset",e2);
    }

    try {
      resetTRA3Model = CommandFactory.getInstance().getCommand("taco:sr/rf-plc/tra3/DevReset");
      resetTRA3Model.addErrorListener(ErrorPopup.getInstance());
    } catch (DevFailed e1) {
      ErrorPane.showErrorMessage(this,"taco:sr/rf-plc/tra3/reset",e1);
    } catch (ConnectionException e2) {
      ErrorPane.showErrorMessage(this,"taco:sr/rf-plc/tra3/reset",e2);
    }
    
    // TRA0 -----------------------------------------------------------------------------------------

    JPanel TRA0Panel = new JPanel();
    TRA0Panel.setLayout(null);
    TRA0Panel.setBorder( BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "TRA0",
                         TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION,
                         borderFont, Color.BLACK) );
    //add(TRA0Panel);

    JLabel TRA0SLab = new JLabel("State");
    TRA0SLab.setBounds(10,20,100,25);
    TRA0Panel.add(TRA0SLab);
    TRA0_state = new SimpleScalarViewer();
    TRA0_state.setBackgroundColor(Color.WHITE);
    TRA0_state.setBounds(110,20,140,25);
    TRA0Panel.add(TRA0_state);

    JLabel TRA0MLab = new JLabel("Mode");
    TRA0MLab.setBounds(10,50,100,25);
    TRA0Panel.add(TRA0MLab);
    TRA0_mode = new SimpleScalarViewer();
    TRA0_mode.setBackgroundColor(Color.WHITE);
    TRA0_mode.setBounds(110,50,140,25);
    TRA0Panel.add(TRA0_mode);

    JLabel TRA0WLab = new JLabel("W.G. mode");
    TRA0WLab.setBounds(10,80,100,25);
    TRA0Panel.add(TRA0WLab);
    TRA0_wg_mode = new SimpleScalarViewer();
    TRA0_wg_mode.setBounds(110,80,140,25);
    TRA0_wg_mode.setBackgroundColor(Color.WHITE);
    try {
      TRA0_wg_mode.setModel((IStringScalar)attList.add("sr/rf-switch/manager/TRA0_mode_str"));
    } catch (ConnectionException e ) {}
    TRA0Panel.add(TRA0_wg_mode);

    TRA0_Combo = new ModeComboBox();
    TRA0_Combo.addItem(new ModeComboItem("Dismount",0));
    TRA0_Combo.addItem(new ModeComboItem("Dummy",3));
    TRA0_Combo.addItem(new ModeComboItem("Short Circuit",4));
    TRA0_Combo.addItem(new ModeComboItem("HVPS Only",5));
    TRA0_Combo.addItem(new ModeComboItem("Anode Only",6));
    TRA0_Combo.addItem(new ModeComboItem("2 Cav cond",7));
    TRA0_Combo.addItem(new ModeComboItem("SYRF Cav",1));
    TRA0_Combo.setBounds(110,110,140,25);
    TRA0_Combo.addModeComboListener(this);
    TRA0Panel.add(TRA0_Combo);

    // TRA1 -----------------------------------------------------------------------------------------

    JPanel TRA1Panel = new JPanel();
    TRA1Panel.setLayout(null);
    TRA1Panel.setBorder( BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "TRA1",
                         TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION,
                         borderFont, Color.BLACK) );
    add(TRA1Panel);

    JLabel TRA1SLab = new JLabel("State");
    TRA1SLab.setBounds(10,20,100,25);
    TRA1Panel.add(TRA1SLab);
    TRA1_state = new SimpleScalarViewer();
    TRA1_state.setBackgroundColor(Color.WHITE);
    TRA1_state.setBounds(110,20,140,25);
    try {
      TRA1_state.setModel((IStringScalar)attList.add("sr/rf-tra/tra1/ShortStatus"));
    } catch (ConnectionException e ) {}
    TRA1Panel.add(TRA1_state);

    JLabel TRA1MLab = new JLabel("Mode");
    TRA1MLab.setBounds(10,50,100,25);
    TRA1Panel.add(TRA1MLab);
    TRA1_mode = new SimpleScalarViewer();
    TRA1_mode.setBackgroundColor(Color.WHITE);
    TRA1_mode.setBounds(110,50,140,25);
    try {
      TRA1_mode.setModel((IStringScalar)attList.add("sr/rf-tra/tra1/Transmitter_mode"));
    } catch (ConnectionException e ) {}
    TRA1Panel.add(TRA1_mode);

    JLabel TRA1WLab = new JLabel("W.G. mode");
    TRA1WLab.setBounds(10,80,100,25);
    TRA1Panel.add(TRA1WLab);
    TRA1_wg_mode = new SimpleScalarViewer();
    TRA1_wg_mode.setBounds(110,80,140,25);
    TRA1_wg_mode.setBackgroundColor(Color.WHITE);
    try {
      TRA1_wg_mode.setModel((IStringScalar)attList.add("sr/rf-switch/manager/TRA1_mode_str"));
    } catch (ConnectionException e ) {}
    TRA1Panel.add(TRA1_wg_mode);

    TRA1_Combo = new ModeComboBox();
    TRA1_Combo.addItem(new ModeComboItem("Dismount",0));
    TRA1_Combo.addItem(new ModeComboItem("2 Cavities",1));
    TRA1_Combo.addItem(new ModeComboItem("4 Cavities",2));
    TRA1_Combo.addItem(new ModeComboItem("Dummy",3));
    TRA1_Combo.addItem(new ModeComboItem("Short Circuit",4));
    TRA1_Combo.addItem(new ModeComboItem("HVPS Only",5));
    TRA1_Combo.addItem(new ModeComboItem("Anode Only",6));
    TRA1_Combo.addItem(new ModeComboItem("2 Cav cond",7));
    TRA1_Combo.addItem(new ModeComboItem("4 Cav cond",8));
    TRA1_Combo.setBounds(110,110,140,25);
    TRA1_Combo.addModeComboListener(this);
    TRA1Panel.add(TRA1_Combo);

    // TRA2 -----------------------------------------------------------------------------------------

    JPanel TRA2Panel = new JPanel();
    TRA2Panel.setLayout(null);
    TRA2Panel.setBorder( BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "TRA2",
                         TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION,
                         borderFont, Color.BLACK) );
    add(TRA2Panel);

    JLabel TRA2SLab = new JLabel("State");
    TRA2SLab.setBounds(10,20,100,25);
    TRA2Panel.add(TRA2SLab);
    TRA2_state = new SimpleScalarViewer();
    TRA2_state.setBackgroundColor(Color.WHITE);
    TRA2_state.setBounds(110,20,140,25);
    try {
      TRA2_state.setModel((IStringScalar)attList.add("sr/rf-tra/tra2/ShortStatus"));
    } catch (ConnectionException e ) {}
    TRA2Panel.add(TRA2_state);

    JLabel TRA2MLab = new JLabel("Mode");
    TRA2MLab.setBounds(10,50,100,25);
    TRA2Panel.add(TRA2MLab);
    TRA2_mode = new SimpleScalarViewer();
    TRA2_mode.setBackgroundColor(Color.WHITE);
    TRA2_mode.setBounds(110,50,140,25);
    try {
      TRA2_mode.setModel((IStringScalar)attList.add("sr/rf-tra/tra2/Transmitter_mode"));
    } catch (ConnectionException e ) {}
    TRA2Panel.add(TRA2_mode);

    JLabel TRA2WLab = new JLabel("W.G. mode");
    TRA2WLab.setBounds(10,80,100,25);
    TRA2Panel.add(TRA2WLab);
    TRA2_wg_mode = new SimpleScalarViewer();
    TRA2_wg_mode.setBounds(110,80,140,25);
    TRA2_wg_mode.setBackgroundColor(Color.WHITE);
    try {
      TRA2_wg_mode.setModel((IStringScalar)attList.add("sr/rf-switch/manager/TRA2_mode_str"));
    } catch (ConnectionException e ) {}
    TRA2Panel.add(TRA2_wg_mode);

    TRA2_Combo = new ModeComboBox();
    TRA2_Combo.addItem(new ModeComboItem("Dismount",0));
    TRA2_Combo.addItem(new ModeComboItem("2 Cavities",1));
    TRA2_Combo.addItem(new ModeComboItem("4 Cavities",2));
    TRA2_Combo.addItem(new ModeComboItem("Dummy",3));
    TRA2_Combo.addItem(new ModeComboItem("Short Circuit",4));
    TRA2_Combo.addItem(new ModeComboItem("HVPS Only",5));
    TRA2_Combo.addItem(new ModeComboItem("Anode Only",6));
    TRA2_Combo.addItem(new ModeComboItem("2 Cav cond",7));
    TRA2_Combo.addItem(new ModeComboItem("4 Cav cond",8));
    TRA2_Combo.addItem(new ModeComboItem("SY Cavities",9));
    TRA2_Combo.addItem(new ModeComboItem("Test stand access",10));
    TRA2_Combo.addItem(new ModeComboItem("Test stand powered",11));
    TRA2_Combo.setBounds(110,110,140,25);
    TRA2_Combo.addModeComboListener(this);
    TRA2Panel.add(TRA2_Combo);

    // TRA3 -----------------------------------------------------------------------------------------

    JPanel TRA3Panel = new JPanel();
    TRA3Panel.setLayout(null);
    TRA3Panel.setBorder( BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "TRA3",
                         TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION,
                         borderFont, Color.BLACK) );
    add(TRA3Panel);

    JLabel TRA3SLab = new JLabel("State");
    TRA3SLab.setBounds(10,20,100,25);
    TRA3Panel.add(TRA3SLab);
    TRA3_state = new SimpleScalarViewer();
    TRA3_state.setBackgroundColor(Color.WHITE);
    TRA3_state.setBounds(110,20,140,25);
    try {
      TRA3_state.setModel((IStringScalar)attList.add("sr/rf-tra/tra3/ShortStatus"));
    } catch (ConnectionException e ) {}
    TRA3Panel.add(TRA3_state);

    JLabel TRA3MLab = new JLabel("Mode");
    TRA3MLab.setBounds(10,50,100,25);
    TRA3Panel.add(TRA3MLab);
    TRA3_mode = new SimpleScalarViewer();
    TRA3_mode.setBackgroundColor(Color.WHITE);
    TRA3_mode.setBounds(110,50,140,25);
    try {
      TRA3_mode.setModel((IStringScalar)attList.add("sr/rf-tra/tra3/Transmitter_mode"));
    } catch (ConnectionException e ) {}
    TRA3Panel.add(TRA3_mode);

    JLabel TRA3WLab = new JLabel("W.G. mode");
    TRA3WLab.setBounds(10,80,100,25);
    TRA3Panel.add(TRA3WLab);
    TRA3_wg_mode = new SimpleScalarViewer();
    TRA3_wg_mode.setBounds(110,80,140,25);
    TRA3_wg_mode.setBackgroundColor(Color.WHITE);
    try {
      TRA3_wg_mode.setModel((IStringScalar)attList.add("sr/rf-switch/manager/TRA3_mode_str"));
    } catch (ConnectionException e ) {}
    TRA3Panel.add(TRA3_wg_mode);

    TRA3_Combo = new ModeComboBox();
    TRA3_Combo.addItem(new ModeComboItem("Dismount",0));
    TRA3_Combo.addItem(new ModeComboItem("Cavity 6",1));
    TRA3_Combo.addItem(new ModeComboItem("Dummy",3));
    TRA3_Combo.addItem(new ModeComboItem("Short Circuit",4));
    TRA3_Combo.addItem(new ModeComboItem("HVPS Only",5));
    TRA3_Combo.addItem(new ModeComboItem("Anode Only",6));
    TRA3_Combo.addItem(new ModeComboItem("Cavity 5",7));
    TRA3_Combo.setBounds(110,110,140,25);
    TRA3_Combo.addModeComboListener(this);
    TRA3Panel.add(TRA3_Combo);

    // State reading
    try {
      TRA1_state_model = (IDevStateScalar)attList.add("sr/rf-tra/tra1/State");
      TRA1_state_model.addDevStateScalarListener(this);
      TRA1_state_model.refresh();
    } catch (ConnectionException e ) {}

    try {
      TRA2_state_model = (IDevStateScalar)attList.add("sr/rf-tra/tra2/State");
      TRA2_state_model.addDevStateScalarListener(this);
      TRA2_state_model.refresh();
    } catch (ConnectionException e ) {}

    try {
      TRA3_state_model = (IDevStateScalar)attList.add("sr/rf-tra/tra3/State");
      TRA3_state_model.addDevStateScalarListener(this);
      TRA3_state_model.refresh();
    } catch (ConnectionException e ) {}


    // Place panels

    //TRA0Panel.setBounds(5,5,260,145);
    //TRA1Panel.setBounds(265,5,260,145);
    //TRA2Panel.setBounds(525,5,260,145);
    //TRA3Panel.setBounds(785,5,260,145);

    TRA1Panel.setBounds(5,5,260,145);
    TRA2Panel.setBounds(265,5,260,145);
    TRA3Panel.setBounds(525,5,260,145);

    // Initialise ModeCombo
    enableWrite = false;

    try {
      TRA0_mode_model = (INumberScalar) AttributeFactory.getInstance().getAttribute("sr/rf-switch/manager/TRA0_mode");
      TRA0_mode_model.addSetErrorListener(ErrorPopup.getInstance());
      TRA0_mode_model.refresh();
      TRA0_Combo.setMode((int)TRA0_mode_model.getNumberScalarValue());
    } catch (DevFailed e1) {
      ErrorPane.showErrorMessage(this,"sr/rf-switch/manager/TRA0_mode",e1);
    } catch (ConnectionException e2) {
      ErrorPane.showErrorMessage(this,"sr/rf-switch/manager/TRA0_mode",e2);
    }

    try {
      TRA1_mode_model = (INumberScalar) AttributeFactory.getInstance().getAttribute("sr/rf-switch/manager/TRA1_mode");
      TRA1_mode_model.addSetErrorListener(ErrorPopup.getInstance());
      TRA1_mode_model.refresh();
      TRA1_Combo.setMode((int)TRA1_mode_model.getNumberScalarValue());
    } catch (DevFailed e1) {
      ErrorPane.showErrorMessage(this,"sr/rf-switch/manager/TRA1_mode",e1);
    } catch (ConnectionException e2) {
      ErrorPane.showErrorMessage(this,"sr/rf-switch/manager/TRA1_mode",e2);
    }

    try {
      TRA2_mode_model = (INumberScalar) AttributeFactory.getInstance().getAttribute("sr/rf-switch/manager/TRA2_mode");
      TRA2_mode_model.addSetErrorListener(ErrorPopup.getInstance());
      TRA2_mode_model.refresh();
      TRA2_Combo.setMode((int)TRA2_mode_model.getNumberScalarValue());
    } catch (DevFailed e1) {
      ErrorPane.showErrorMessage(this,"sr/rf-switch/manager/TRA2_mode",e1);
    } catch (ConnectionException e2) {
      ErrorPane.showErrorMessage(this,"sr/rf-switch/manager/TRA2_mode",e2);
    }

    try {
      TRA3_mode_model = (INumberScalar) AttributeFactory.getInstance().getAttribute("sr/rf-switch/manager/TRA3_mode");
      TRA3_mode_model.addSetErrorListener(ErrorPopup.getInstance());
      TRA3_mode_model.refresh();
      TRA3_Combo.setMode((int)TRA3_mode_model.getNumberScalarValue());
    } catch (DevFailed e1) {
      ErrorPane.showErrorMessage(this,"sr/rf-switch/manager/TRA3_mode",e1);
    } catch (ConnectionException e2) {
      ErrorPane.showErrorMessage(this,"sr/rf-switch/manager/TRA3_mode",e2);
    }

    enableWrite = true;

  }

  public void modeChange(ModeComboBox src) {

    if( src==TRA0_Combo ) {
      ModeComboItem it = (ModeComboItem)src.getSelectedItem();
      traModeChange();
    } else if ( src==TRA1_Combo ) {
      ModeComboItem it = (ModeComboItem)src.getSelectedItem();
      traModeChange();
    } else if ( src==TRA2_Combo ) {
      ModeComboItem it = (ModeComboItem)src.getSelectedItem();
      traModeChange();
    } else if ( src==TRA3_Combo ) {
      ModeComboItem it = (ModeComboItem)src.getSelectedItem();
      traModeChange();
    }

  }

  private void traModeChange() {

    TRA0_Combo.enableAll();
    TRA1_Combo.enableAll();
    TRA2_Combo.enableAll();
    TRA3_Combo.enableAll();

    int tra0Mode = TRA0_Combo.getMode();
    int tra1Mode = TRA1_Combo.getMode();
    int tra2Mode = TRA2_Combo.getMode();
    int tra3Mode = TRA3_Combo.getMode();

    // Disable menu item to avoid invalid WG configuration
    switch(tra0Mode) {
      case 1: // 2 Cav
      case 7: // 2 Cav cond
        TRA2_Combo.disableItem(9);
        break;
    }

    switch(tra1Mode) {
      case 1: // 2 Cav
      case 7: // 2 Cav cond
        TRA2_Combo.disableItem(2);
        TRA2_Combo.disableItem(8);
        break;
      case 2: // 4 cav
      case 8: // 4 cav cond
        TRA2_Combo.disableItem(1);
        TRA2_Combo.disableItem(7);
        TRA2_Combo.disableItem(2);
        TRA2_Combo.disableItem(8);
        break;
    }

    switch(tra2Mode) {
      case 1: // 2 Cav
      case 7: // 2 Cav cond
        TRA1_Combo.disableItem(2);
        TRA1_Combo.disableItem(8);
        break;
      case 2: // 4 cav
      case 8: // 4 cav cond
        TRA1_Combo.disableItem(1);
        TRA1_Combo.disableItem(7);
        TRA1_Combo.disableItem(2);
        TRA1_Combo.disableItem(8);
        break;
      case 9:
        TRA0_Combo.disableItem(1);
        TRA0_Combo.disableItem(7);
        break;
    }

    // Write mode to the DS
    if( enableWrite ) {
      TRA0_mode_model.setValue((double)tra0Mode);
      TRA1_mode_model.setValue((double)tra1Mode);
      TRA2_mode_model.setValue((double)tra2Mode);
      TRA3_mode_model.setValue((double)tra3Mode);
    }

  }

  public void actionPerformed(ActionEvent e) {

    Object src = e.getSource();

    if(src==sendBtn) {
      SendToPLC();
    }

  }

  private void SendToPLC() {

    //Check TRA2 on test stand powered
    if( TRA2_Combo.getMode()==0xB ) {

      if( JOptionPane.showConfirmDialog(this,"Make sure that the search has been made in the test stand,\n"
                                             +"otherwise the beam will be killed.\n"
                                             +"Do you really want to send the configuration to the PLC ?",
                                             "Save confirmation",JOptionPane.YES_NO_OPTION)==JOptionPane.NO_OPTION) {
        return;
      }

    }

    sendModel.execute();

    // Reset transmitter
    try {
      Thread.sleep(1000);
    } catch(Exception e) {}

    resetTRA1Model.execute();
    resetTRA2Model.execute();
    resetTRA3Model.execute();

  }

  public void toggleExpertMode() {

    if( expertMode ) {

      expertMode=false;
      expertModeViewer.setToggleBackground(Color.WHITE);
      
    } else {

      if( JOptionPane.showConfirmDialog(null,"You will have access to all menus.\n"
                                             +"Do you want to switch to expert mode ?",
                                             "Expert Mode Confirmation",JOptionPane.YES_NO_OPTION)==JOptionPane.NO_OPTION) {
        return;
      }

      expertMode=true;
      expertModeViewer.setToggleBackground(Color.ORANGE);

    }
    enablePanelItem();

  }

  public void devStateScalarChange(DevStateScalarEvent evt) {

    Object src = evt.getSource();
    String state = evt.getValue();

    if (src == TRA1_state_model) {
      TRA1_stt = state;
    } else if (src == TRA2_state_model) {
      TRA2_stt = state;
    } else if (src == TRA3_state_model) {
      TRA3_stt = state;
    }

    enablePanelItem();

  }

  public void stateChange(AttributeStateEvent e)
  {
  }

  public void errorChange(ErrorEvent evt)
  {

    Object src = evt.getSource();

    if( src==TRA1_state_model ) {
      TRA1_stt = IDevice.UNKNOWN;
    } else if ( src==TRA2_state_model ) {
      TRA2_stt = IDevice.UNKNOWN;
    } else if ( src==TRA3_state_model ) {
      TRA3_stt = IDevice.UNKNOWN;
    }

    enablePanelItem();

  }

  private void enablePanelItem() {

    TRA0_Combo.setEnabled(true);
    sendBtn.setEnabled(true);

    // Check transmitter mode
    if (!expertMode) {

      if (TRA1_stt.equalsIgnoreCase(IDevice.ON) ||
          TRA1_stt.equalsIgnoreCase(IDevice.ALARM) ) {
        TRA1_Combo.setEnabled(false);
      } else {
        TRA1_Combo.setEnabled(true);
      }

      if (TRA2_stt.equalsIgnoreCase(IDevice.ON) ||
          TRA2_stt.equalsIgnoreCase(IDevice.ALARM) ) {
        TRA2_Combo.setEnabled(false);
      } else {
        TRA2_Combo.setEnabled(true);
      }

      if (TRA3_stt.equalsIgnoreCase(IDevice.ON) ||
          TRA3_stt.equalsIgnoreCase(IDevice.ALARM)) {
        TRA3_Combo.setEnabled(false);
      } else {
        TRA3_Combo.setEnabled(true);
      }
              
    } else {

      // Expert mode
      TRA1_Combo.setEnabled(true);
      TRA2_Combo.setEnabled(true);
      TRA3_Combo.setEnabled(true);

    }

  }

}
