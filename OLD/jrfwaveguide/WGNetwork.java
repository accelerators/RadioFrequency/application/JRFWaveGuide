package jrfwaveguide;

import fr.esrf.tangoatk.widget.util.jdraw.JDObject;
import fr.esrf.tangoatk.widget.util.jdraw.JDrawEditor;

import java.awt.*;
import java.util.Vector;


class NetWorkItem {

  NetWorkItem() {
    wg = null;
    wgSwitch = null;
    next1 = null;
    next2 = null;
  }

  JDObject wg;           // The wave guide
  JDObject wgSwitch;     // Wage guide switch (at the end of the wg)

  NetWorkItem next1;     // The next wage guide (wg_switch position 1)
  NetWorkItem next2;     // The next waveguide  (wg_switch position 2)

}

/**
 * A class that compute RF path in the wage guide network
 */
public class WGNetwork {

  JDrawEditor jde;

  // Starting node
  NetWorkItem TRA1;
  NetWorkItem TRA2;
  NetWorkItem TRA3;

  WGNetwork(JDrawEditor jde) {

    this.jde = jde;

    // Construct the network wave guide

    NetWorkItem S281_SC = new NetWorkItem();
    S281_SC.wg = getObjectByName("w_S281_SC");

    NetWorkItem S282_SC = new NetWorkItem();
    S282_SC.wg = getObjectByName("w_S282_SC");

    NetWorkItem S272_S28 = new NetWorkItem();
    S272_S28.wg = getObjectByName("w_S272_S28");
    S272_S28.wgSwitch = getObjectByName("sr/rf-switch/S28/Position");
    S272_S28.next1 = S281_SC;
    S272_S28.next2 = S282_SC;

    NetWorkItem S15_CAV12 = new NetWorkItem();
    S15_CAV12.wg = getObjectByName("w_S15_CAV12");

    NetWorkItem S14_S151 = new NetWorkItem();
    S14_S151.wg = getObjectByName("w_S14_S151");
    S14_S151.wgSwitch = getObjectByName("sr/rf-switch/S15/Position");
    S14_S151.next1 = S15_CAV12;
    
    NetWorkItem T_S141 = new NetWorkItem();
    T_S141.wg = getObjectByName("w_T_S141");
    T_S141.wgSwitch = getObjectByName("sr/rf-switch/S14/Position");
    T_S141.next1 = S14_S151;

    NetWorkItem S132_S142 = new NetWorkItem();
    S132_S142.wg = getObjectByName("w_S132_S142");
    S132_S142.wgSwitch = getObjectByName("sr/rf-switch/S14/Position");
    S132_S142.next2 = S14_S151;

    NetWorkItem S26_CAV34 = new NetWorkItem();
    S26_CAV34.wg = getObjectByName("w_S26_CAV34");

    NetWorkItem S24_S261 = new NetWorkItem();
    S24_S261.wg = getObjectByName("w_S24_S261");
    S24_S261.wgSwitch = getObjectByName("sr/rf-switch/S26/Position");
    S24_S261.next1 = S26_CAV34;

    NetWorkItem T_S242 = new NetWorkItem();
    T_S242.wg = getObjectByName("w_T_S242");
    T_S242.wgSwitch = getObjectByName("sr/rf-switch/S24/Position");
    T_S242.next2 = S24_S261;

    NetWorkItem S131_T = new NetWorkItem();
    S131_T.wg = getObjectByName("w_S131_T");
    S131_T.next1 = T_S141;
    S131_T.next2 = T_S242;

    NetWorkItem S12_S13 = new NetWorkItem();
    S12_S13.wg = getObjectByName("w_S12_S13");
    S12_S13.wgSwitch = getObjectByName("sr/rf-switch/S13/Position");
    S12_S13.next1 = S131_T;
    S12_S13.next2 = S132_S142;

    NetWorkItem S271_S122 = new NetWorkItem();
    S271_S122.wg = getObjectByName("w_S271_S122");
    S271_S122.wgSwitch = getObjectByName("sr/rf-switch/S12/Position");
    S271_S122.next2 = S12_S13;

    NetWorkItem S212_S27 = new NetWorkItem();
    S212_S27.wg = getObjectByName("w_S212_S27");
    S212_S27.wgSwitch = getObjectByName("sr/rf-switch/S27/Position");
    S212_S27.next1 = S271_S122;
    S212_S27.next2 = S272_S28;

    NetWorkItem S251_TS = new NetWorkItem();
    S251_TS.wg = getObjectByName("w_S251_TS");

    NetWorkItem S252_SC = new NetWorkItem();
    S252_SC.wg = getObjectByName("w_S252_SC");

    NetWorkItem S232_S25 = new NetWorkItem();
    S232_S25.wg = getObjectByName("w_S232_S25");
    S232_S25.wgSwitch = getObjectByName("sr/rf-switch/S25/Position");
    S232_S25.next1 = S251_TS;
    S232_S25.next2 = S252_SC;

    NetWorkItem S231_S241 = new NetWorkItem();
    S231_S241.wg = getObjectByName("w_S231_S241");
    S231_S241.wgSwitch = getObjectByName("sr/rf-switch/S24/Position");
    S231_S241.next1 = S24_S261;

    NetWorkItem S222_S23 = new NetWorkItem();
    S222_S23.wg = getObjectByName("w_S222_S23");
    S222_S23.wgSwitch = getObjectByName("sr/rf-switch/S23/Position");
    S222_S23.next1 = S231_S241;
    S222_S23.next2 = S232_S25;

    NetWorkItem S221_D = new NetWorkItem();
    S221_D.wg = getObjectByName("w_S221_D");

    NetWorkItem S211_S22 = new NetWorkItem();
    S211_S22.wg = getObjectByName("w_S211_S22");
    S211_S22.wgSwitch = getObjectByName("sr/rf-switch/S22/Position");
    S211_S22.next1 = S221_D;
    S211_S22.next2 = S222_S23;

    TRA2 = new NetWorkItem();
    TRA2.wg = getObjectByName("w_tra2_S21");
    TRA2.wgSwitch = getObjectByName("sr/rf-switch/S21/Position");
    TRA2.next1 = S211_S22;
    TRA2.next2 = S212_S27;

    NetWorkItem S112_D = new NetWorkItem();
    S112_D.wg = getObjectByName("w_S112_D");

    NetWorkItem S111_S121 = new NetWorkItem();
    S111_S121.wg = getObjectByName("w_S111_S121");
    S111_S121.wgSwitch = getObjectByName("sr/rf-switch/S12/Position");
    S111_S121.next1 = S12_S13;
    
    TRA1 = new NetWorkItem();
    TRA1.wg = getObjectByName("w_tra1_S11");
    TRA1.wgSwitch = getObjectByName("sr/rf-switch/S11/Position");
    TRA1.next1 = S111_S121;
    TRA1.next2 = S112_D;

    NetWorkItem S32_CAV56 = new NetWorkItem();
    S32_CAV56.wg = getObjectByName("w_S32_CAV56");

    NetWorkItem S311_S321 = new NetWorkItem();
    S311_S321.wg = getObjectByName("w_S311_S321");
    S311_S321.wgSwitch = getObjectByName("sr/rf-switch/S32/Position");
    S311_S321.next1 = S32_CAV56;

    NetWorkItem S312_D = new NetWorkItem();
    S312_D.wg = getObjectByName("w_S312_D");

    TRA3 = new NetWorkItem();
    TRA3.wg = getObjectByName("w_tra3_S31");
    TRA3.wgSwitch = getObjectByName("sr/rf-switch/S31/Position");
    TRA3.next1 = S311_S321;
    TRA3.next2 = S312_D;

  }

  private JDObject getObjectByName(String name) {

    Vector v = jde.getObjectsByName(name,false);

    if( v.size()==0 ) {
      System.out.println("Warning: object "+name+" not found");
      return null;
    }

    if(v.size()>1) {
      System.out.println("Warning: several objects have the same name: "+name);
    }

    return (JDObject)v.get(0);

  }

  public void computeWGStates() {

    resetNetwork();
    browseNetwork(TRA1);
    browseNetwork(TRA2);
    browseNetwork(TRA3);
    jde.repaint();

  }

  private void resetNetwork() {

    for(int i=0;i<jde.getObjectNumber();i++) {
      JDObject o = jde.getObjectAt(i);
      if(o.getName().startsWith("w_")) {
        o.setBackground(Color.WHITE);
      }
    }

  }

  private void browseNetwork(NetWorkItem n) {

    n.wg.setBackground(Color.GREEN);

    if(n.wgSwitch!=null) {

      int v = n.wgSwitch.getValue();
      if(v==1 && n.next1!=null) browseNetwork(n.next1);
      if(v==2 && n.next2!=null) browseNetwork(n.next2);

    } else {

      // Special case for the magic tee
      if(n.next1!=null && n.next2!=null) {
        browseNetwork(n.next1);
        browseNetwork(n.next2);
      }

    }
    
  }

}
